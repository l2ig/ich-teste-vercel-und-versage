import mysql from "serverless-mysql";

const db = mysql({
    config: {
        host: "linters.de",
        database: "test",
        user: "test",
        password: "test"
    }
});

export default async function query(statement) {
    try {
        const results = await db.query(statement);
        await db.end();
        return results;
    }catch(error) {
        return [];
    }
};