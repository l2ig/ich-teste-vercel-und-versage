const fetch = require("node-fetch");
const mysql = require("mysql2");

(async () => {
    const connection = mysql.createConnection({
        host: "linters.de",
        user: "test",
        password: "test",
        database: "test"
    });
    const bitcoinPrices = await (await fetch("https://blockchain.info/ticker")).json();

    connection.query(`insert into bitcoin_prices(USD, EUR) values('${bitcoinPrices["USD"]["last"]}', '${bitcoinPrices["EUR"]["last"]}')`);
})();