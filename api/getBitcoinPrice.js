import fetch from "node-fetch";
import query from "../lib/db";

export default async (req, res) => {
    const bitcoinPrices = await (await fetch("https://blockchain.info/ticker")).json();

    await query(`insert into bitcoin_prices(USD, EUR) values('${bitcoinPrices["USD"]["last"]}', '${bitcoinPrices["EUR"]["last"]}')`);
    res.json(bitcoinPrices);
}