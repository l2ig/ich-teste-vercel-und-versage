import React from "react";
import "./assets/styles/tailwind.css";

export default function App() {
    const [bitcoinPrice, setBitcoinPrice] = React.useState(0);
    const [isLoading, setIsLoading] = React.useState(false);

    const fetchBitcoinPrice = () => {
        setBitcoinPrice(0);
        setIsLoading(true);
        fetch("api/getBitcoinPrice").then(res => res.json()).then(bitcoinPrices => {
            setBitcoinPrice(bitcoinPrices["USD"]["last"]);
            setIsLoading(false);
        });
    };

    return (
        <div className="App p-8">
            <button onClick={fetchBitcoinPrice} className="mt-4 p-3 bg-gray-300 rounded">Bitcoin-Preis laden</button>
            <div className="p-4 bg-gray-300 mt-4 w-64">
                {isLoading
                    ? "Lädt..."
                    : bitcoinPrice === 0
                        ? "Kein Bitcoin-Preis verfügbar"
                        : "$" + bitcoinPrice}
            </div>
        </div>
    );
};